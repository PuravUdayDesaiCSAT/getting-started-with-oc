# Getting Started with OC

You need to install 3 software:
- hidapi
- openocd
- gdb

## Install **hidapi**

Execute the following commands:

- `
sudo apt-get install libudev-dev
`

- `
git clone git://github.com/signal11/hidapi.git
`

- `
cd hidapi
`

- `
./bootstrap
`

- `
sudo apt install cmake
`

- `
sudo apt-get install libusb-1.0-0-dev
`

- `
./configure
` ( Make sure that after running _./configure_ there are no errors, if any error occurs then search for the same on the net )

- `
make
`

- `
sudo make install
`

![hidapi](hidapi.png)

After successful installation go to the parent directory of _hidapi_.

## Install **openocd**

Note: **DO NOT** install openocd under hidapi folder

1. **To obtain openocd source code execute the following commands:**

    - `
git clone git://git.code.sf.net/p/openocd/code openocd
`

    - `
cd openocd
`

    - `
sudo apt-get build-dep openocd
`

    - `
./bootstrap
`

    - `
./configure
`

You can verify your installation by the below screenshot

![openocd](openocd.png)

2. **Execute the following command:** `
sudo apt-get install libftdi-dev libftdi1
`

3. **Create a file _/etc/udev/rules.d/olimex-arm-usb-tiny-h.rules_**

You can use the following commands to do so:

- `
cd /etc/udev/rules.d/
`

- `
sudo touch olimex-arm-usb-tiny-h.rules
`

4. **Add this line in that file `SUBSYSTEM=="usb", ACTION=="add", ATTR{idProduct}=="002a", ATTR{idVendor}=="15ba", MODE="664", GROUP="plugdev"
`**

You can use the following commands to do so:

- `
vim touch olimex-arm-usb-tiny-h.rules
`

- Press **i** - You will go in INSERT mode

- Paste the above line

5. **Go to the parent directory of _where you installed openocd source code_**

6. **Execute the following commands:**

- `
sudo apt-get install git libtool automake texinfo
`

- `
cd openocd
`

- (OPTIONAL) You can check the different versions provided : 
`
git tag -l
`

- For OC we need version 10, execute this command to do so 
`
git checkout v0.10.0
`

- `
./bootstrap
`

- `
./configure --enable-maintainer-mode --enable-ft2232_libftdi --disable-werror
`

- `
make
`

- `
sudo make install
`

You can verify your installation by the below screenshot

![openocd version](openocd_version.png)

**NOTE:**
By mistakenly if you install different version of _openocd_ other than **0.10.0** then execute below steps:

- go to _openocd_ folder
- `sudo apt-get remove openocd 
`
- `sudo apt-get remove --auto-remove openocd 
`
- `sudo apt-get purge openocd 
`
- `sudo apt-get purge --auto-remove openocd 
`
- `sudo make uninstall
`
- `make -n install
`
- `make clean
`
- reinstall openocd again

## Install **gdb toolchain**

Execute the following commands:

- `sudo apt-get update
`
- `sudo apt remove gcc-arm-none-eabi
`
- Download the latest _tar.bz2_ file from [here](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads)
- Unzip it using `sudo tar xjf gcc-arm-none-eabi-<your_version>.bz2 -C /usr/share/
` (sudo tar xjf gcc-arm-none-eabi-10.3-2021.07-x86_64-linux.tar.bz2 -C /usr/share/)

- `sudo ln -s /usr/share/gcc-arm-none-eabi-your-version/bin/arm-none-eabi-gcc /usr/bin/arm-none-eabi-gcc 
`
- `sudo ln -s /usr/share/gcc-arm-none-eabi-your-version/bin/arm-none-eabi-g++ /usr/bin/arm-none-eabi-g++
`
- `sudo ln -s /usr/share/gcc-arm-none-eabi-your-version/bin/arm-none-eabi-gdb /usr/bin/arm-none-eabi-gdb
`
- `sudo ln -s /usr/share/gcc-arm-none-eabi-your-version/bin/arm-none-eabi-size /usr/bin/arm-none-eabi-size
`
- `sudo apt install libncurses5
`
-  `sudo apt install libncurses-dev
`
- `sudo ln -s /usr/lib/x86_64-linux-gnu/libncurses.so.6 /usr/lib/x86_64-linux-gnu/libncurses.so.5
`
- `sudo ln -s /usr/lib/x86_64-linux-gnu/libtinfo.so.6 /usr/lib/x86_64-linux-gnu/libtinfo.so.5
`
- `sudo apt-get remove binutils-arm-none-eabi gcc-arm-none-eabi
`
- `sudo add-apt-repository ppa:team-gcc-arm-embedded/ppa
`
- `sudo apt-get install gcc-arm-none-eabi
`

Go to `/usr/share
`
- `sudo rm arm-none-eabi-gdb
`
- `sudo rm arm-none-eabi-objcopy
`

Go to the following directory `/gcc-arm-none-eabi-10.3-2021.07/bin
`
- `sudo cp arm-none-eabi-gdb /usr/bin
`
- `sudo cp arm-none-eabi-objcopy /usr/bin
`

Check installation by `arm-none-eabi-gdb
` command

You can verify your installation by the below screenshot

![gdb](gdb.png)

### References

**hidapi**

- [https://jjmilburn.github.io/2014/09/18/Atmel-SAMD20-EclipseCDT/](https://jjmilburn.github.io/2014/09/18/Atmel-SAMD20-EclipseCDT/)

openocd

- [https://beckustech.wordpress.com/2012/10/01/configuring-openocd-with-an-olimex-arm-usb-tiny-h-in-ubuntu-12-04-64-bit/](https://beckustech.wordpress.com/2012/10/01/configuring-openocd-with-an-olimex-arm-usb-tiny-h-in-ubuntu-12-04-64-bit/)

gdb

- [https://launchpad.net/~team-gcc-arm-embedded/+archive/ubuntu/ppa](https://launchpad.net/~team-gcc-arm-embedded/+archive/ubuntu/ppa)
- [https://itectec.com/ubuntu/ubuntu-how-to-install-arm-none-eabi-gdb-on-ubuntu-20-04-lts-focal-fossa/](https://itectec.com/ubuntu/ubuntu-how-to-install-arm-none-eabi-gdb-on-ubuntu-20-04-lts-focal-fossa/)
- [https://www.youtube.com/watch?v=hpvlvtNc-sU&t=71s](https://www.youtube.com/watch?v=hpvlvtNc-sU&t=71s)
